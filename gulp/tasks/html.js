import fileinclude from "gulp-file-include"
import versionNumber from 'gulp-version-number'

export const html = () => {
  return app.gulp.src(app.path.src.html)
    .pipe(app.plugins.plumber(
      app.plugins.notify.onError({
        title: 'HTML',
        message: 'Error: <%= error.message %>'
      })
    ))
    .pipe(fileinclude())
    .pipe(app.plugins.replace(/\.\.\/images/g, "images"))
    .pipe(app.gulp.dest(app.path.build.html))
    .pipe(app.plugins.browserSync.stream())
}